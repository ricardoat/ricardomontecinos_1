<?php
	$title = "Texturas - Ricardo Montecinos";
	include('index.php');
?>


<main>


<div id="photos">
	<div id="textIni">
	<p>"El verdadero amor de un hombre no se encuentra en el <br>
hombre mismo, sino en los colores y texturas <br>
que cobran vida en otros" <br>
<br>
Albert Schweitzer</p>
	</div>
<img src="images/textura/1.JPG" alt="imagen">
<img src="images/textura/2.JPG" alt="imagen">
<img src="images/textura/3.JPG" alt="imagen">
<img src="images/textura/4.JPG" alt="imagen">
<img src="images/textura/5.JPG" alt="imagen">
<img src="images/textura/6.JPG" alt="imagen">
<img src="images/textura/7.jpg" alt="imagen">
<img src="images/textura/8.JPG" alt="imagen">
<img src="images/textura/9.JPG" alt="imagen">
</div>

</main>
<?php
    include('footer.php');
?>
