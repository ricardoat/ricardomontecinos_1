<?php
	$title = "Toulouse - Ricardo Montecinos";
	include('index.php');
?>


<main>


<div id="photos">
	<div id="textIni">
	<p>"Estamos aquí para matar la guerra. <br>
Estamos aquí para reirnos del destino y reir tan <br>
bien nuestra vida que la muerte tiemble al recibirnos" <br>
<br>
Charles Bukowski</p>

</div>
<img src="images/toulouse/1.jpg" alt="imagen">
<img src="images/toulouse/2.jpg" alt="imagen">
<img src="images/toulouse/3.jpg" alt="imagen">
<img src="images/toulouse/4.jpg" alt="imagen">
<img src="images/toulouse/5.jpg" alt="imagen">
<img src="images/toulouse/6.jpg" alt="imagen">
<img src="images/toulouse/7.jpg" alt="imagen">
<img src="images/toulouse/8.jpg" alt="imagen">
<img src="images/toulouse/9.jpg" alt="imagen">
</div>

</main>
<?php
    include('footer.php');
?>
