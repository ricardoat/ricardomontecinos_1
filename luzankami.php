<?php
	$title = "Stadion za Lužánkami - Ricardo Montecinos";
	include('index.php');
?>


<main>


<div id="photos">
	<div id="textIni">
	<p>"Lo análogo envejece, se pudre <br>
mengua de generación en generación <br>
cambia su sonido, su apariencia, su olor" <br>
<br>
Fred Ritchin</p>
	</div>
<img src="images/luzankami/1.jpg" alt="imagen">
<img src="images/luzankami/2.jpg" alt="imagen">
<img src="images/luzankami/3.jpg" alt="imagen">
<img src="images/luzankami/4.jpg" alt="imagen">
<img src="images/luzankami/5.jpg" alt="imagen">
<img src="images/luzankami/6.jpg" alt="imagen">
<img src="images/luzankami/7.jpg" alt="imagen">
<img src="images/luzankami/8.jpg" alt="imagen">
<img src="images/luzankami/9.jpg" alt="imagen">
</div>

</main>
<?php
    include('footer.php');
?>
