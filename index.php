<!DOCTYPE html>
<html lang="en">
<head>
  <title><?php echo $title; ?></title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="robots" content="noindex, nofollow">
  <meta name="googlebot" content="noindex, nofollow">
  <link rel="shortcut icon" href="images/favi.ico">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <meta name="description" content="Photographer">
  <meta name="keywords" content="Photography, Documentary, art">
  <meta name="author" content="Ricardo Montecinos">
  <meta property="og:image" content="/images/home_670.png"/>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js?ver=1.3.2'></script>
  <script type='text/javascript' src='js/jquery.mousewheel.min.js'></script>
  <script type='text/javascript' src='js/scroll.js'></script>
</head>
<body>

<header>
<div id="header">
<a href="home.php"><img src="images/header.png" alt="header"></a>
</div>
</header>

<div id="relleno"></div>

<nav>
  <div id="menu">
  <ul class="nav">
    <li><a id="proyecto" href="#">Proyectos</a>
      <ul id="sm1">
        <li><a href="abstracc.php">Abstracciones</a></li>
        <li><a href="ceniza.php">Ceniza</a></li>
        <li><a href="marie.php">Encuentro con Mary</a></li>
        <li><a href="lena.php">Olor a Leña</a></li>
        <li><a href="latino.php">Latino en 16:9</a></li>
        <li><a href="luzankami.php">Za Luzankami</a></li>
        <li><a href="texturas.php">Texturas</a></li>
        <li><a href="toulouse.php">Toulouse</a></li>
      </ul>
    </li>
    <li><a href="trabajos.php">Trabajos</a></li>
    <li><a href="video.php">Video</a></li>
    <li><a href="https://www.instagram.com/ricmontecinos/" target="_blank">Instagram</a></li>
    <li><a href="about.php">Contacto</a></li>
  </ul>
</div>
<script type='text/javascript' src='js/index.js'></script>

</nav>
