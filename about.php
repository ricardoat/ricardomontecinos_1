<?php
	$title = "Ricardo Montecinos";
	include('index.php');
?>


<main>


<div id="photos">

<div >
  <p>Soy <strong>Ricardo Montecinos Molina</strong>.<br>
  Además de fotógrafo, tengo estudios en periodismo, software y  robótica.<br>
  También cuento con experiencia en el mundo de las artes y en el diseño gráfico. <br>
  En la actualidad paso mis días entre la fotografía, el diseño web y el diseño e impresión 3D. <br>
  Pueden ver mi CV <a style="text-decoration: underline" href="cv_montecinos.pdf" target="_blank">aquí</a>.<br>
<br>
<br>
<br>
  <strong>CONTACTO</strong><br>
  rmontecinosmolina@gmail.com <br>
  Buenos Aires, Argentina.</p>
</div>

</div>

</main>



<?php
    include('footer.php');
?>
